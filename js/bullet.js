function Bullet(x,y,tarX, tarY){
	this.sprite = game.add.sprite(x,y,'bullet');
    game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

    this.sprite.checkWorldBounds = true;
    this.sprite.events.onOutOfBounds.add(function(b){b.kill();}, this);

    var dx = tarX-x;
    var dy = tarY-y;
    var mag = Math.abs(Math.sqrt(dx*dx + dy*dy));
	this.sprite.body.velocity.x = 500 * (dx/mag);
	this.sprite.body.velocity.y = 500 * (dy/mag);

	this.update = function(){
		if(!this.sprite.alive){
			return false;
		}
		return true;
	}
}
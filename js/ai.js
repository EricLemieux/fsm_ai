function AI(x,y){
	this.sprite = game.add.sprite(x,y,'monster');	
    game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

    this.sprite.checkWorldBounds = true;
    this.sprite.events.onOutOfBounds.add(function(b){b.kill();}, this);
    
    var chaseThreshold = 200;
	this.update = function(){
		if(this.sprite.alive){
			//Call the different states here
			var dx = player.x - this.sprite.x;
			var dy = player.y - this.sprite.y;
			var mag = Math.abs(Math.sqrt(dx*dx + dy*dy));

			if(mag < chaseThreshold){	//Attack state
				this.sprite.x += 2 * (dx/mag);
				this.sprite.y += 2 * (dy/mag);
			}else{	//Idle state
				this.sprite.x += rand(0,6)-2;
				this.sprite.y += rand(0,6)-2;
			}

			return true;
		}
		return false;
	}

}
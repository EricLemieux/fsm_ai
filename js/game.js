var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update});

var player;
var playerHealth = 100;
var currentlyShooting = false;

var enemies = [];
var bullets = [];

const playerMoveSpeed = 4;

function preload(){
	game.load.image('player', 'img/player.png');
	game.load.image('bullet', 'img/bullet.png');
	game.load.image('monster', 'img/monster.png');
}

function create(){
	game.physics.startSystem(Phaser.Physics.ARCADE);

	game.stage.backgroundColor = '#4a8848';

	player = game.add.sprite(400,300,'player');
	player.anchor.setTo(0.5);
    game.physics.enable([player], Phaser.Physics.ARCADE);
}

function update(){
	if(game.input.keyboard.isDown(Phaser.Keyboard.UP) || game.input.keyboard.isDown(Phaser.Keyboard.W)){
		player.y -= playerMoveSpeed;
	}else if(game.input.keyboard.isDown(Phaser.Keyboard.DOWN) || game.input.keyboard.isDown(Phaser.Keyboard.S)){
		player.y += playerMoveSpeed;
	}

	if(game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || game.input.keyboard.isDown(Phaser.Keyboard.A)){
		player.x -= playerMoveSpeed;
	}else if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || game.input.keyboard.isDown(Phaser.Keyboard.D)){
		player.x += playerMoveSpeed;
	}

	if(game.input.activePointer.leftButton.isDown && !currentlyShooting){
		var b = new Bullet(player.x, player.y, game.input.x, game.input.y);
		bullets.push(b);
		currentlyShooting = true;
	}else{
		currentlyShooting = false;
	}

	//TODO change this so they spawn on their own
	if(game.input.keyboard.isDown(Phaser.Keyboard.Q)){
		spawnNewEnemy();
	}

	while(enemies.length < 20){
		spawnNewEnemy();
	}

	//Set the player to face the mouse
	player.angle = game.physics.arcade.angleBetween(player, game.input) * (180/Math.PI);

	//Update all of the enemies
	var enemiesToRemove = [];
	for(var i =0; i < enemies.length; ++i){
		game.physics.arcade.collide(player, enemies[i].sprite, null, hitPlayer, this);
		if(!enemies[i].update()){
			enemiesToRemove.push(i);
		}

		var bulletsToRemove = [];
		for(var j = 0; j < bullets.length; ++j){
			game.physics.arcade.collide(bullets[j].sprite, enemies[i].sprite, null, hitBullet, this);
			if(!bullets[j].update()){
				bulletsToRemove.push(j);
			}
		}
		for(var k = bulletsToRemove.length-1; k >= 0; --k){
			bullets.splice(bulletsToRemove[k],1);
		}
	}
	for(var k = enemiesToRemove.length-1; k >= 0; --k){
		enemies.splice(enemiesToRemove[k],1);
	}
}

function rand(min, max){
	return Math.floor((Math.random() * max) + min);
}

function spawnNewEnemy(){
	var enemy = new AI(rand(0,800), rand(0,600));
	enemies.push(enemy);
}

function hitPlayer(obj1, obj2){
	playerHealth -= 10;
	if(playerHealth < 0){
		//alert("Game Over");
	}
}

function hitBullet(obj1, obj2){
	obj1.kill();
	obj2.kill();
}